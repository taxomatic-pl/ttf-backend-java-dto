package pl.ttfinance.backend.accounting.shared;

import lombok.RequiredArgsConstructor;
import pl.ttfinance.backend.common.date.Quarter;

import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.ToIntFunction;

@RequiredArgsConstructor
public enum AccountingPeriodType {
    MONTH(
            date -> date.getMonthValue(),
            (year, period) -> LocalDate.of(year, period, 1),
            (year, period) -> LocalDate.of(year, period, 1).with(TemporalAdjusters.lastDayOfMonth()),
            period -> period >= 1 && period <= 12,
            date -> date.plusMonths(1)
    ),
    QUARTER(
            date -> date.get(IsoFields.QUARTER_OF_YEAR),
            (year, period) -> Quarter.from(period).getFrom().atYear(year),
            (year, period) -> Quarter.from(period).getTo().atYear(year),
            period -> period >= 1 && period <= 4,
            date -> date.plusMonths(3)
    );

    private final ToIntFunction<LocalDate> toPeriod;
    private final BiFunction<Integer, Integer, LocalDate> toStartOfPeriod;
    private final BiFunction<Integer, Integer, LocalDate> toEndOfPeriod;
    private final IntPredicate periodValidator;
    private final Function<LocalDate, LocalDate> nextPeriod;

    public LocalDate nextPeriod(LocalDate period) {
        return nextPeriod.apply(period);
    }

    public LocalDate toStartOfPeriod(LocalDate date) {
        return toStartOfPeriod(date.getYear(), toPeriod(date));
    }

    public LocalDate toStartOfPeriod(int year, int period) {
        if (!isValid(period)) {
            throw new IllegalArgumentException();
        }

        return toStartOfPeriod.apply(year, period);
    }

    public LocalDate toEndOfPeriod(LocalDate date) {
        return toEndOfPeriod(date.getYear(), toPeriod(date));
    }

    public LocalDate toEndOfPeriod(int year, int period) {
        if (!isValid(period)) {
            throw new IllegalArgumentException();
        }

        return toEndOfPeriod.apply(year, period);
    }

    public int toPeriod(LocalDate date) {
        return toPeriod.applyAsInt(date);
    }

    public boolean isValid(int period) {
        return periodValidator.test(period);
    }
}
