package pl.ttfinance.backend.accounting.shared;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;

import static java.lang.Long.parseLong;
import static lombok.AccessLevel.PROTECTED;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode
@ToString
public class AccountingYearId {
    public static final String FIELD = "id";

    @JsonValue
    private long id;

    public AccountingYearId(String id) {
        this.id = parseLong(id);
    }

    public long id() {
        return id;
    }
}
