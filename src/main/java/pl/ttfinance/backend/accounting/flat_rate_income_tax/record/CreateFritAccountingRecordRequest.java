package pl.ttfinance.backend.accounting.flat_rate_income_tax.record;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@SuperBuilder
public class CreateFritAccountingRecordRequest extends FritAccountingRecordMessage {

}
