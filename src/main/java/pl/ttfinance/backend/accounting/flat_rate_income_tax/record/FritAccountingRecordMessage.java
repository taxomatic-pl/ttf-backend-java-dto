package pl.ttfinance.backend.accounting.flat_rate_income_tax.record;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import pl.nethos.elasticsearch.annotation.meta.EsDateField;
import pl.nethos.elasticsearch.annotation.meta.EsNestedField;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.value.FritValues;
import pl.ttfinance.backend.elasticsearch.ttf_definition.annotation.TaxoEsTextField;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
@FieldNameConstants
@Getter
@SuperBuilder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class FritAccountingRecordMessage {
    @EsDateField
    @NonNull
    @NotNull
    LocalDate transactionOn;

    @EsDateField
    @NonNull
    @NotNull
    LocalDate incomeTaxObligationOn;

    @TaxoEsTextField
    @NonNull
    @NotNull
    @NotEmpty
    String number;

    @TaxoEsTextField
    String description;

    @TaxoEsTextField
    @NonNull
    @NotNull
    @NotEmpty
    String buyerName;

    @TaxoEsTextField
    @NonNull
    @NotNull
    @NotEmpty
    String buyerAddress;

    @EsNestedField
    @NonNull
    @NotNull
    @Valid
    FritValues values;
}
