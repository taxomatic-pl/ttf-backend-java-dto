package pl.ttfinance.backend.accounting.flat_rate_income_tax.record;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import pl.ttfinance.backend.common.jpa.RevisedKey;

import javax.validation.constraints.NotNull;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@SuperBuilder
public class UpdateFritAccountingRecordRequest extends FritAccountingRecordMessage {
    @NonNull
    @NotNull
    RevisedKey key;
}
