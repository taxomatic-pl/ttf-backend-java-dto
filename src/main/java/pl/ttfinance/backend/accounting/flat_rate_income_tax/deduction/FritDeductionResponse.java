package pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.accounting.shared.AccountingYearId;
import pl.ttfinance.backend.common.money.TaxoValue;

import java.time.LocalDate;

@Value
@Builder
public class FritDeductionResponse {
    long id;
    long version;
    @NonNull TaxoValue incomeDeduction;
    @NonNull TaxoValue taxDeduction;
    @NonNull LocalDate period;
    @NonNull AccountingYearId yearId;
}
