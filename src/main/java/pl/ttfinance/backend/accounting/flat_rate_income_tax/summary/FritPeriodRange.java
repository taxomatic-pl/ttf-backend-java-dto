package pl.ttfinance.backend.accounting.flat_rate_income_tax.summary;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.accounting.shared.AccountingPeriodType;
import pl.ttfinance.backend.common.date.DateRange;

import java.time.LocalDate;

@Value
public class FritPeriodRange {
    int yearFrom;
    int yearTo;

    int periodFrom;
    int periodTo;

    AccountingPeriodType type;

    public FritPeriodRange(LocalDate from, LocalDate to, AccountingPeriodType type) {
        this(
                from.getYear(),
                to.getYear(),
                type.toPeriod(from),
                type.toPeriod(to),
                type
        );
    }

    @Builder
    private FritPeriodRange(
            int yearFrom,
            int yearTo,
            int periodFrom,
            int periodTo,
            @NonNull AccountingPeriodType type
    ) {
        if (yearFrom > yearTo) {
            throw new IllegalArgumentException();
        }

        if (yearFrom == yearTo && periodFrom > periodTo) {
            throw new IllegalArgumentException();
        }

        if (!type.isValid(periodFrom) || !type.isValid(periodTo)) {
            throw new IllegalArgumentException();
        }

        this.yearFrom = yearFrom;
        this.yearTo = yearTo;
        this.periodFrom = periodFrom;
        this.periodTo = periodTo;
        this.type = type;
    }

    public DateRange toDateRange() {
        return new DateRange(
                type.toStartOfPeriod(yearFrom, periodFrom),
                type.toEndOfPeriod(yearTo, periodTo)
        );
    }
}
