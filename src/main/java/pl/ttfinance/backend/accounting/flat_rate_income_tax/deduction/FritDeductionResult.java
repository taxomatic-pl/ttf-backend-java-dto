package pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction;

import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.common.money.TaxoValue;

@Value
public class FritDeductionResult {
    @NonNull TaxoValue leftToDeductFrom;
    @NonNull TaxoValue leftToDeduct;
    @NonNull FritDeductionRow deduction;
}
