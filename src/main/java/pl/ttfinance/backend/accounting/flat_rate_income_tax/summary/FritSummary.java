package pl.ttfinance.backend.accounting.flat_rate_income_tax.summary;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction.FritDeductionRow;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.value.FritValues;
import pl.ttfinance.backend.common.date.DateRange;
import pl.ttfinance.backend.common.money.TaxoValue;

@Value
@Builder
public class FritSummary {
    @NonNull DateRange period;
    @NonNull FritPeriod periodKey;

    @NonNull FritValues percentages;

    @NonNull FritValues income;
    @NonNull TaxoValue incomeTotal;

    @NonNull FritDeductionRow socialSecurityDeduction;
    @NonNull FritDeductionRow otherIncomeDeduction;
    @NonNull FritValues incomeDeduction;
    @NonNull TaxoValue incomeDeductionTotal;

    @NonNull FritValues incomeAfterDeduction;
    @NonNull TaxoValue incomeAfterDeductionTotal;

    @NonNull FritValues taxBeforeAnyDeductions;
    @NonNull TaxoValue taxBeforeAnyDeductionsTotal;

    @NonNull FritValues taxAfterIncomeDeduction;
    @NonNull TaxoValue taxAfterIncomeDeductionTotal;

    @NonNull FritDeductionRow healthInsuranceDeduction;
    @NonNull FritDeductionRow otherTaxDeduction;
    @NonNull TaxoValue taxDeduction;
    @NonNull TaxoValue taxAfterAllDeductions;
}
