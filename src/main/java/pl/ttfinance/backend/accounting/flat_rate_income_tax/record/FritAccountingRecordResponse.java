package pl.ttfinance.backend.accounting.flat_rate_income_tax.record;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import pl.nethos.elasticsearch.annotation.EsIndex;
import pl.nethos.elasticsearch.annotation.meta.EsDecimalField;
import pl.nethos.elasticsearch.annotation.meta.EsLongField;
import pl.nethos.elasticsearch.annotation.meta.EsObjectField;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.summary.FritPeriod;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.summary.FritPeriodicRecord;
import pl.ttfinance.backend.accounting.shared.AccountingMonthId;
import pl.ttfinance.backend.accounting.shared.AccountingPeriodType;
import pl.ttfinance.backend.accounting.shared.AccountingYearId;
import pl.ttfinance.backend.common.jpa.EntityAudit;
import pl.ttfinance.backend.common.jpa.RevisedKey;
import pl.ttfinance.backend.common.money.TaxoValue;
import pl.ttfinance.backend.group.shared.GroupId;

@EsIndex(type = "FritAccountingRecord")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@SuperBuilder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FritAccountingRecordResponse extends FritAccountingRecordMessage {
    @EsObjectField
    @NonNull
    RevisedKey key;

    @EsObjectField
    RevisedKey incomeKey;

    @EsLongField
    @NonNull
    GroupId groupId;

    @EsObjectField
    @NonNull
    EntityAudit audit;

    @EsLongField
    @NonNull
    FritAccountingRecordOrdinalDto ordinal;

    @EsDecimalField
    @NonNull
    TaxoValue incomeTotal;

    @EsDecimalField
    @NonNull
    TaxoValue taxTotal;

    @EsLongField
    @NonNull
    AccountingMonthId monthId;

    @EsLongField
    @NonNull
    AccountingYearId yearId;

    public FritPeriodicRecord toPeriodicRecord(AccountingPeriodType type) {
        FritPeriod key = new FritPeriod(getIncomeTaxObligationOn(), type);
        return new FritPeriodicRecord(key, getValues());
    }
}
