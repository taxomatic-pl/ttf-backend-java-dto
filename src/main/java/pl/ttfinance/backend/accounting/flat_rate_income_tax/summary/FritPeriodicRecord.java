package pl.ttfinance.backend.accounting.flat_rate_income_tax.summary;

import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.value.FritValues;

@Value
public class FritPeriodicRecord {
    @NonNull
    FritPeriod period;

    @NonNull
    FritValues values;

    public FritPeriodicRecord add(FritPeriodicRecord other) {
        if (!period.equals(other.period)) {
            throw new IllegalArgumentException("Different period keys: " + period + " other key: " + other.period);
        }

        return new FritPeriodicRecord(period, values.add(other.values));
    }

    public static FritPeriodicRecord zero(FritPeriod period) {
        return new FritPeriodicRecord(period, FritValues.ZERO);
    }
}
