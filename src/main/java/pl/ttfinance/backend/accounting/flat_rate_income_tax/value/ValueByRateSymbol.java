package pl.ttfinance.backend.accounting.flat_rate_income_tax.value;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import pl.nethos.elasticsearch.annotation.meta.EsDecimalField;
import pl.nethos.elasticsearch.annotation.meta.EsKeywordField;
import pl.ttfinance.backend.common.money.TaxoValue;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@EqualsAndHashCode
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ValueByRateSymbol {
    @EsKeywordField
    private String rateSymbol;

    @EsDecimalField
    private TaxoValue value;

    public ValueByRateSymbol(@NonNull FritRate rate, @NonNull TaxoValue value) {
        this(rate.getSymbol(), value);
    }
}
