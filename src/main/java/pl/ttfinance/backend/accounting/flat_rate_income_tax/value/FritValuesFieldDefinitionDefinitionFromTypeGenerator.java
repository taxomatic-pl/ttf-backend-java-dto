package pl.ttfinance.backend.accounting.flat_rate_income_tax.value;

import pl.nethos.elasticsearch.annotation.FieldDefinitionFromTypeGenerator;
import pl.nethos.elasticsearch.definition.FieldDefinition;
import pl.nethos.elasticsearch.definition.FieldType;

import java.util.Arrays;
import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class FritValuesFieldDefinitionDefinitionFromTypeGenerator implements FieldDefinitionFromTypeGenerator {

    @Override
    public Collection<FieldDefinition> generate(Class<?> type) {
        return Arrays.stream(FritRate.values())
                .map(rate -> FieldDefinition.builder()
                        .name(rate.getEsName())
                        .type(FieldType.DOUBLE)
                        .build()
                )
                .collect(toList());
    }
}
