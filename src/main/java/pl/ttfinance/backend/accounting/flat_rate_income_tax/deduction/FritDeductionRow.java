package pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction;

import lombok.NonNull;
import lombok.Value;
import pl.ttfinance.backend.common.money.TaxoValue;

@Value
public class FritDeductionRow {
    @NonNull TaxoValue current;
    @NonNull TaxoValue previous;
    @NonNull TaxoValue deducted;
}
