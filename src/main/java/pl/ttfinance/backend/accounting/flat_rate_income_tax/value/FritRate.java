package pl.ttfinance.backend.accounting.flat_rate_income_tax.value;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.NoSuchElementException;

// copy of flat_rate_income_tax.json dictionary from ttf-dictionary project.
@Getter
@RequiredArgsConstructor
public enum FritRate {
    _0020("0020", "2%", new BigDecimal("0.020")),
    _0030("0030", "2%", new BigDecimal("0.030")),
    _0055("0055", "5,5%", new BigDecimal("0.055")),
    _0085("0085", "8,5%", new BigDecimal("0.085")),
    _0100("0100", "10%", new BigDecimal("0.100")),
    _0150("0150", "15%", new BigDecimal("0.150")),
    _0170("0170", "17%", new BigDecimal("0.170")),
    _0200("0200", "20%", new BigDecimal("0.200")),
    _0125("0125", "12,5%", new BigDecimal("0.125")),
    _0120("0120", "12%", new BigDecimal("0.120")),
    _0140("0140", "14%", new BigDecimal("0.140"));

    private static final String ES_NAME_PREFIX = "rate";

    private final String symbol;
    private final String label;
    private final BigDecimal value;

    @JsonValue
    public String getEsName() {
        return ES_NAME_PREFIX + symbol;
    }

    public static FritRate fromEsName(@NonNull String esName) {
        return fromSymbol(esName.replace(ES_NAME_PREFIX, ""));
    }

    public static FritRate fromSymbol(@NonNull String symbol) {
        return Arrays.stream(values())
                .filter(e -> e.getSymbol().equalsIgnoreCase(symbol))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No rate for symbol: " + symbol));
    }

}
