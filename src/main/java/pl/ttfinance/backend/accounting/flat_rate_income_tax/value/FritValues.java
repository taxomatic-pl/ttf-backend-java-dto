package pl.ttfinance.backend.accounting.flat_rate_income_tax.value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.NonNull;
import pl.nethos.elasticsearch.annotation.EsFieldGenerator;
import pl.ttfinance.backend.common.money.TaxoValue;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@EsFieldGenerator(FritValuesFieldDefinitionDefinitionFromTypeGenerator.class)
public final class FritValues {
    public static final FritValues ZERO = FritValues.from(TaxoValue.ZERO);

    @JsonValue
    @NotEmpty
    private Map<String, TaxoValue> values;

    @JsonCreator
    protected FritValues(Map<String, TaxoValue> values) {
        this.values = values;
    }

    public static FritValues from(Collection<ValueByRateSymbol> values) {
        Map<String, TaxoValue> newValues = values.stream()
                .collect(toMap(e -> FritRate.fromSymbol(e.getRateSymbol()).getEsName(), ValueByRateSymbol::getValue, TaxoValue::plus));

        return new FritValues(newValues);
    }

    public static FritValues from(@NonNull Map<FritRate, TaxoValue> values) {
        values.values().stream()
                .filter(Objects::isNull)
                .findFirst()
                .ifPresent(e -> {
                    throw new IllegalArgumentException("No null values allowed");
                });

        Map<String, TaxoValue> valueMap = values.entrySet().stream()
                .collect(toMap(e -> e.getKey().getEsName(), Entry::getValue));

        return new FritValues(valueMap);
    }

    public static FritValues from(@NonNull TaxoValue value) {
        return new FritValues(transformRates(rate -> value));
    }

    private static Map<String, TaxoValue> transformRates(Function<FritRate, TaxoValue> mapper) {
        return Arrays.stream(FritRate.values())
                .collect(toMap(FritRate::getEsName, mapper));
    }

    public TaxoValue get(FritRate rate) {
        return values.getOrDefault(rate.getEsName(), TaxoValue.ZERO);
    }

    public FritValues add(FritValues other) {
        return new FritValues(transformRates(rate -> get(rate).plus(other.get(rate))));
    }

    public FritValues subtract(FritValues subtrahend) {
        return add(subtrahend.negate());
    }

    public FritValues multiply(BigDecimal multiplicand) {
        return new FritValues(transformRates(rate -> get(rate).multiply(multiplicand)));
    }

    public FritValues divide(BigDecimal divisor) {
        return multiply(BigDecimal.ONE.divide(divisor, 20, RoundingMode.HALF_UP));
    }

    public FritValues negate() {
        return multiply(new BigDecimal("-1"));
    }

    public TaxoValue total() {
        return values.values().stream()
                .reduce(TaxoValue.ZERO, TaxoValue::plus)
                .rounded();
    }

    public FritValues toPercentages() {
        TaxoValue total = total();
        if (total.isEqualTo(TaxoValue.ZERO)) {
            return ZERO;
        } else {
            return divide(total.toBigDecimal());
        }
    }

    public FritValues toDeductions(TaxoValue deductionTotal) {
        return toPercentages().multiply(deductionTotal.toBigDecimal()).rounded();
    }

    public FritValues toTax() {
        return new FritValues(transformRates(rate -> get(rate).multiply(rate.getValue()).rounded()));
    }

    public FritValues rounded() {
        return new FritValues(transformRates(rate -> get(rate).rounded()));
    }

    public List<ValueByRateSymbol> toValueByRateSymbol() {
        return values.entrySet().stream()
                .map(e -> new ValueByRateSymbol(FritRate.fromEsName(e.getKey()), e.getValue()))
                .collect(toList());
    }
}
