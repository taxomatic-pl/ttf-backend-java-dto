package pl.ttfinance.backend.accounting.flat_rate_income_tax.summary;

import lombok.Value;
import pl.ttfinance.backend.accounting.shared.AccountingPeriodType;
import pl.ttfinance.backend.common.date.DateRange;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Value
public class FritPeriod implements Comparable<FritPeriod> {
    public static final Comparator<FritPeriod> COMPARATOR = Comparator
            .comparingInt(FritPeriod::getYear)
            .thenComparing(FritPeriod::getPeriod);

    int year;
    int period;
    AccountingPeriodType type;

    public FritPeriod(LocalDate date, AccountingPeriodType type) {
        this(date.getYear(), type.toPeriod(date), type);
    }

    public FritPeriod(int year, int period, AccountingPeriodType type) {
        if (!type.isValid(period)) {
            throw new IllegalArgumentException();
        }

        this.year = year;
        this.period = period;
        this.type = type;
    }

    @Override
    public int compareTo(FritPeriod o) {
        return COMPARATOR.compare(this, o);
    }

    public DateRange toDateRange() {
        return new DateRange(getStart(), getEnd());
    }

    public LocalDate getStart() {
        return type.toStartOfPeriod(year, period);
    }

    public LocalDate getEnd() {
        return type.toEndOfPeriod(year, period);
    }

    public static List<FritPeriod> generate(DateRange range, AccountingPeriodType periodType) {
        LocalDate start = periodType.toStartOfPeriod(range.getFrom());
        LocalDate end = periodType.toEndOfPeriod(range.getTo());

        LocalDate period = start;
        List<FritPeriod> result = new ArrayList<>();
        while (!period.isAfter(end)) {
            FritPeriod fritPeriod = new FritPeriod(period.getYear(), periodType.toPeriod(period), periodType);
            result.add(fritPeriod);
            period = periodType.nextPeriod(period);
        }

        return result;
    }
}
