package pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.ttfinance.backend.accounting.shared.AccountingPeriodType;
import pl.ttfinance.backend.accounting.shared.AccountingYearId;
import pl.ttfinance.backend.common.money.TaxoValue;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CreateOrUpdateFritDeductionRequest {
    @NotNull private TaxoValue incomeDeduction;
    @NotNull private TaxoValue taxDeduction;
    @NotNull private LocalDate period;
    @NotNull private AccountingPeriodType periodType;
    @NotNull private AccountingYearId yearId;
}
