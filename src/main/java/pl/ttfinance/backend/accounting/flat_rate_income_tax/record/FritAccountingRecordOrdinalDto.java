package pl.ttfinance.backend.accounting.flat_rate_income_tax.record;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.Value;

@Value
@Getter
public class FritAccountingRecordOrdinalDto {
    @JsonValue
    long value;

    public FritAccountingRecordOrdinalDto(long value) {
        if (value < 1) {
            throw new IllegalArgumentException("Ordinals lower than 1 are not allowed");
        }

        this.value = value;
    }
}
