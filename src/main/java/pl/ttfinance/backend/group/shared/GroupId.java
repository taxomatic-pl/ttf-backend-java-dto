package pl.ttfinance.backend.group.shared;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;

import static lombok.AccessLevel.PRIVATE;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor(access = PRIVATE)
@EqualsAndHashCode
@ToString
public class GroupId {
	public static final String FIELD = "id";

	@JsonValue
	private long id;

	public GroupId(String id) {
		this(Long.parseLong(id));
	}

	public long id() {
		return id;
	}
}
