package pl.ttfinance.backend.common.date;

import lombok.Value;

import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * All operations in this class assume inclusivity for both from and to parameters - the range is closed on both sides, <from, to>.
 */
@Value
public class DateRange {

	LocalDate from;
	LocalDate to;

	public DateRange(LocalDate from, LocalDate to) {
		if (from.isAfter(to)) {
			throw new IllegalArgumentException("From date should not be after to date.");
		}

		this.from = from;
		this.to = to;
	}

	public List<YearMonth> monthsBetween() {
		YearMonth month = YearMonth.from(from);
		YearMonth toMonth = YearMonth.from(to);
		List<YearMonth> result = new ArrayList<>();
		do {
			result.add(month);
			month = month.plusMonths(1);
		} while (month.compareTo(toMonth) <= 0); // range closed
		return result;
	}

	public List<Year> yearsBetween() {
		return IntStream.rangeClosed(from.getYear(), to.getYear())
				.mapToObj(Year::of)
				.collect(toList());
	}

	public boolean contains(LocalDate date) {
		return !from.isAfter(date) && !to.isBefore(date);
	}

	public boolean contains(DateRange other) {
		return contains(other.from) && contains(other.to);
	}

	public boolean overlaps(DateRange other) {
		return contains(other.from) || contains(other.to) || contains(other);
	}

	public DateRange combine(DateRange other) {
		LocalDate newFrom = from.isBefore(other.from) ? from : other.from;
		LocalDate newTo = to.isAfter(other.to) ? to : other.to;

		return new DateRange(newFrom, newTo);
	}
}
