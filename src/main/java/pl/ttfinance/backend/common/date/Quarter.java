package pl.ttfinance.backend.common.date;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;
import java.time.temporal.IsoFields;

@RequiredArgsConstructor
@Getter
public enum Quarter {
    I(MonthDay.of(Month.JANUARY, 1), MonthDay.of(Month.MARCH, 31)),
    II(MonthDay.of(Month.APRIL, 1), MonthDay.of(Month.JUNE, 30)),
    III(MonthDay.of(Month.JULY, 1), MonthDay.of(Month.SEPTEMBER, 30)),
    IV(MonthDay.of(Month.OCTOBER, 1), MonthDay.of(Month.DECEMBER, 31));

    private final MonthDay from;
    private final MonthDay to;

    public DateRange toRange(Year year) {
        return new DateRange(
                year.atMonthDay(from),
                year.atMonthDay(to)
        );
    }

    public static Quarter from(LocalDate date) {
        return from(date.get(IsoFields.QUARTER_OF_YEAR));
    }

    public static Quarter from(int quarter) {
        return values()[quarter - 1];
    }
}
