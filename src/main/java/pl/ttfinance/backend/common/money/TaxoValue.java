package pl.ttfinance.backend.common.money;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.nethos.money.Constants;
import pl.nethos.money.Currency;
import pl.nethos.money.base.BaseFinancialValue;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction.FritDeductionRow;
import pl.ttfinance.backend.accounting.flat_rate_income_tax.deduction.FritDeductionResult;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@ToString(callSuper = true)
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Embeddable
public final class TaxoValue extends BaseFinancialValue<TaxoValue, TaxoTotals, TaxoMoney> {
    public static final TaxoValue ZERO = new TaxoValue(Constants.ZERO);
    public static final TaxoValue ONE = new TaxoValue(Constants.ONE);

    public TaxoValue(String value) {
        super(value);
    }

    public TaxoValue(Number value) {
        super(value);
    }

    public TaxoValue(Double value) {
        super(value);
    }

    public TaxoValue(Integer value) {
        super(value);
    }

    public TaxoValue(BigDecimal value) {
        super(value);
    }

    @Override
    protected TaxoValue newValue(BigDecimal value) {
        return new TaxoValue(value);
    }

    @Override
    protected TaxoMoney newMoney(TaxoValue value, Currency currency) {
        return new TaxoMoney(value, currency);
    }

    @Override
    protected TaxoTotals newTotals(TaxoValue net, TaxoValue tax, TaxoValue gross) {
        return new TaxoTotals(net, tax, gross);
    }

    public FritDeductionResult deduct(TaxoValue current, TaxoValue previous) {
        TaxoValue deductionTotal = current.plus(previous);
        if (isGreaterThan(deductionTotal)) {
            return new FritDeductionResult(
                    minus(deductionTotal),
                    TaxoValue.ZERO,
                    new FritDeductionRow(current, previous, deductionTotal)
            );
        } else {
            return new FritDeductionResult(
                    TaxoValue.ZERO,
                    deductionTotal.minus(this),
                    new FritDeductionRow(current, previous, this)
            );
        }
    }
}
