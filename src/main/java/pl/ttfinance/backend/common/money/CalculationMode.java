package pl.ttfinance.backend.common.money;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.function.BiFunction;

@RequiredArgsConstructor
public enum CalculationMode {
    gross(TaxoValue::toTotalsAsGross),
    net(TaxoValue::toTotalsAsNet);

    private final BiFunction<TaxoValue, BigDecimal, TaxoTotals> toTotals;

    public TaxoTotals toTotals(TaxoValue value, BigDecimal taxRate) {
        return toTotals.apply(value, taxRate);
    }
}
