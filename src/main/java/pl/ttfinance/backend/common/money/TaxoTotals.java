package pl.ttfinance.backend.common.money;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.nethos.money.base.BaseFinancialTotals;

import javax.persistence.Embeddable;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Embeddable
public final class TaxoTotals extends BaseFinancialTotals<TaxoValue> {
    public TaxoTotals(TaxoValue amountNet, TaxoValue amountTax, TaxoValue amountGross) {
        super(amountNet, amountTax, amountGross);
    }
}
