package pl.ttfinance.backend.common.money;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import pl.nethos.money.Constants;
import pl.nethos.money.Currency;
import pl.nethos.money.base.BaseMoney;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Embeddable
public final class TaxoMoney extends BaseMoney<TaxoValue, TaxoTotals, TaxoMoney> {
    public TaxoMoney(TaxoValue value) {
        this(value, Constants.DEFAULT_CURRENCY);
    }

    public TaxoMoney(BigDecimal value, Currency currency) {
        this(new TaxoValue(value), currency);
    }

    public TaxoMoney(BigDecimal value) {
        this(value, Constants.DEFAULT_CURRENCY);
    }

    public TaxoMoney(Number value, Currency currency) {
        this(new BigDecimal(value.toString()), currency);
    }

    public TaxoMoney(Number value) {
        this(new BigDecimal(value.toString()), Constants.DEFAULT_CURRENCY);
    }

    public TaxoMoney(TaxoValue value, @NonNull Currency currency) {
        super(value, currency);
    }

//    public TaxoMoney tryToDomesticCurrency(BigDecimal exchangeRate) {
//        return getValue().tryToDomesticCurrency(exchangeRate, getCurrency())
//                .toMoney(getCurrency());
//    }
}
