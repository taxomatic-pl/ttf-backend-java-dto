package pl.ttfinance.backend.common.money;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import pl.nethos.elasticsearch.annotation.meta.EsDateField;
import pl.nethos.elasticsearch.annotation.meta.EsDecimalField;
import pl.nethos.elasticsearch.annotation.meta.EsKeywordField;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Embeddable
@EqualsAndHashCode
@Getter
public class CurrencyExchangeRate {
    @NotNull
    @NonNull
    @EsDecimalField
    private BigDecimal rate;

    @EsDateField
    private LocalDate publishedOn;

    @EsKeywordField
    private String recordNumber;
}
