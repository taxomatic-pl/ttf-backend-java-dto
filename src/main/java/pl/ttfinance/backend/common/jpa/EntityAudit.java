package pl.ttfinance.backend.common.jpa;

import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import pl.nethos.elasticsearch.annotation.meta.EsDateTimeField;
import pl.nethos.elasticsearch.annotation.meta.EsLongField;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Instant;

@Embeddable
@Getter
@ToString
public class EntityAudit {

    @EsDateTimeField
    @Column(name = "CREATED_AT", updatable = false)
    @CreatedDate
    private Instant createdAt;

    @EsLongField
    @Column(name = "CREATED_BY", updatable = false)
    @CreatedBy
    private Long createdBy;

    @EsDateTimeField
    @Column(name = "MODIFIED_AT")
    @LastModifiedDate
    private Instant modifiedAt;

    @EsLongField
    @Column(name = "MODIFIED_BY")
    @LastModifiedBy
    private Long modifiedBy;
}

