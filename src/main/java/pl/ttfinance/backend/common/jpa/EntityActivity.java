package pl.ttfinance.backend.common.jpa;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

import static lombok.AccessLevel.PRIVATE;

@Embeddable
@Getter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(access = PRIVATE)
public class EntityActivity {

	public static final EntityActivity ACTIVE = new EntityActivity(true, false, false);

	private boolean headRevision;

	private boolean removed;

	private boolean archived;
}
