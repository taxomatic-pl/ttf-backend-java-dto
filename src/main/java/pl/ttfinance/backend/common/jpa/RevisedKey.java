package pl.ttfinance.backend.common.jpa;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.nethos.elasticsearch.annotation.meta.EsLongField;

import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@MappedSuperclass
@ToString
public class RevisedKey implements Serializable {
    public static final String ID = "id";
    public static final String REVISION = "revision";

    @EsLongField
    private long id;

    @EsLongField
    private long revision;
}
