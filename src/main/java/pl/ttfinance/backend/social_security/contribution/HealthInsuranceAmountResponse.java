package pl.ttfinance.backend.social_security.contribution;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class HealthInsuranceAmountResponse {

	BigDecimal value;

	BigDecimal toDeduct;
}
