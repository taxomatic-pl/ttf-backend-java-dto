package pl.ttfinance.backend.social_security.contribution;

import lombok.Builder;
import lombok.Value;
import pl.ttfinance.backend.common.jpa.RevisedKey;
import pl.ttfinance.backend.common.money.TaxoValue;
import pl.ttfinance.backend.group.shared.GroupId;
import pl.ttfinance.backend.user.shared.UserId;

import java.time.LocalDate;
import java.time.OffsetDateTime;

@Value
@Builder
public class SocialSecurityContributionResponse {
    RevisedKey key;
    GroupId groupId;
    UserId userId;
    OffsetDateTime createdAt;
    LocalDate period;
    LocalDate paymentOn;
    TaxoValue pension;
    TaxoValue disabilityBenefits;
    TaxoValue sicknessBenefits;
    TaxoValue unemploymentFund;
    TaxoValue accidentInsurance;
    TaxoValue healthInsurance;
    TaxoValue healthInsuranceToDeduct;

    public TaxoValue getSocialBenefit() {
        return pension
                .plus(disabilityBenefits)
                .plus(accidentInsurance)
                .plus(sicknessBenefits);
    }
}
