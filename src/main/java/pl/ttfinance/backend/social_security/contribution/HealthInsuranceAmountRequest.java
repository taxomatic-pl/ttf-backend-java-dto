package pl.ttfinance.backend.social_security.contribution;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HealthInsuranceAmountRequest {

	private LocalDate period;
}
