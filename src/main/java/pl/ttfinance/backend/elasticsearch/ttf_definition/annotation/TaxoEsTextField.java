package pl.ttfinance.backend.elasticsearch.ttf_definition.annotation;

import pl.nethos.elasticsearch.annotation.EsField;
import pl.nethos.elasticsearch.annotation.EsSubfield;
import pl.nethos.elasticsearch.definition.Analyzers;
import pl.nethos.elasticsearch.definition.FieldType;
import pl.nethos.elasticsearch.definition.Normalizers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@EsField(type = FieldType.TEXT, analyzer = Analyzers.MORFOLOGIK, sortBy = "raw", fields = {
        @EsSubfield(name = "raw", type = FieldType.KEYWORD, normalizer = Normalizers.CASE_INSENSITIVE),
        @EsSubfield(name = "standard", type = FieldType.TEXT, analyzer = Analyzers.STANDARD),
        @EsSubfield(name = "simple", type = FieldType.TEXT, analyzer = Analyzers.SIMPLE),
        @EsSubfield(name = "en", type = FieldType.TEXT, analyzer = Analyzers.ENGLISH)
})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TaxoEsTextField {
}
